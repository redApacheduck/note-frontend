import axios from 'axios'

const baseUrl = 'https://studies.cs.helsinki.fi/restcountries'

const getAll = () => {
  return axios.get(baseUrl)
}

const getCountry = (country) => {
    const promise = axios.get(`${baseUrl}/api/name/${country}`)
    return promise
}

export default {
    getCountry : getCountry,
    getAll : getAll,
}
