import { useState } from "react"

export const Feedback = () => {
    const [feedbackStats,setStats] = useState({
        good : 0,
        neutral : 0,
        bad : 0,
      })
    return (
        <p>
              <GiveFeedback handleClick={(feedback)=>{ 
          const newState = {
            ...feedbackStats,
          }
          newState[feedback] = feedbackStats[feedback] + 1
          setStats(newState) 
      }}></GiveFeedback>
      <FeedbackStats stats={feedbackStats}></FeedbackStats>
        </p>
    )
}
const GiveFeedback = (props) =>{
    return (
      <p>
        <h1>Give feedback</h1>
        <StatButton text='good' handleClick={() => props.handleClick('good')}></StatButton>
        <StatButton text='neutral' handleClick={() => props.handleClick('neutral')}></StatButton>
        <StatButton text='bad' handleClick={() => props.handleClick('bad')}></StatButton>
      </p>
    )
  }

  const FeedbackStats = (props) => {
    if(props.stats.good +props.stats.neutral+props.stats.bad===0){
      return(
        <p>
          No feedback given
        </p>
      )
    }else{
      return (
        <table>
          <StatsLine text='Good' value={props.stats.good}></StatsLine>
          <StatsLine text='Neutral' value={props.stats.neutral}></StatsLine>
          <StatsLine text='Bad' value={props.stats.bad}></StatsLine>
          <StatsLine text='All' value={props.stats.good +props.stats.neutral+props.stats.bad}></StatsLine>
          <StatsLine text='Average' value={((props.stats.good +props.stats.neutral+props.stats.bad)/3).toFixed(2)}></StatsLine>
          <StatsLine text='Positive' value={((props.stats.good)/(props.stats.good +props.stats.neutral+props.stats.bad)* 100).toFixed(2) + '%'}></StatsLine>
        </table>
      )
    } 
  }


const StatButton = ({text,handleClick}) =>{
    return (
      <button onClick={() => handleClick(text)}>
        {text}
      </button>
    )
  }
  const StatsLine = ({text,value}) =>{
    return (
      <tr>
        <td>{text}</td>
        <td>{value}</td>
      </tr>
    )
  }