export const Courses = ({courses}) => {
  const course = [
    {
      name : 'Half Stack application development',
      parts : [
        {
          name: 'Fundamentals of React' ,
          exercises : 10,
          id : 1,
        },
        {
          name: 'Using props to pass data' ,
          exercises : 7,
          id : 2,
        },
        {
          name: 'State of a component' ,
          exercises : 14,
          id : 3,
        },
        {
          name: 'Redux' ,
          exercises : 11,
          id : 4,
        },
      ]
    },
    {
      name: 'Node.js',
      id: 2,
      parts: [
        {
          name: 'Routing',
          exercises: 3,
          id: 1
        },
        {
          name: 'Middlewares',
          exercises: 7,
          id: 2
        }
      ]
    }
  ]


  
  return (
    <div>
      {course.map(c =>{
        return <Course course={c}></Course>
      })}
      <br></br>
    </div>
  )
}
const Course = ({course}) => {
    
  
  return(
      <p>
         <Header name={course.name}></Header>
        <Content prop={course.parts}></Content>
        <Total parts={course.parts}></Total>
      </p>
    )
  }
  
  const Total = (props) =>{
    return (
      <p>Number of exercises {props.parts.reduce((acc,element) => acc + element.exercises,0)}</p>
    )
  }
  
  const Part = (prop) => {
    return (
      <p>
        {prop.name} {prop.exercises}
      </p>
    )
  }
  const Content = ({prop}) => {
    return (
      <p>
        {prop.map(element =>{
          return <Part name={element.name} exercises={element.exercises}></Part>
        })}
       
      </p>
    )
  }
  const Header = (prop) =>{
    return (
      <p><h1>{prop.name}</h1></p>
    )
  }

