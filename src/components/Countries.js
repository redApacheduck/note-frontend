import service from './../services/countries'
import { useEffect, useState } from 'react'
export const Countries = () => {
    
    const [country,setCountry] = useState(null)

    useEffect(()=>{
        service
            .getCountry(country)
            .then(result =>{
                console.log(`result ${JSON.stringify(result)}`)
            })
            .catch(error => {
                console.log(`result ${error}`)
            })
    },[country])
    return (
        <div>
            find countries 
            <form>
                <input value={country} onChange={event => setCountry(event.target.value)}></input>
            </form>
        </div>
    )
}