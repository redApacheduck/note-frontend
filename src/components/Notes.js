import { useState,useEffect } from "react"
import noteService from './../services/notes'
export const Notes = (props) => {
  const [notes, setNotes] = useState(props.notes)
  const [newNote, setNewNote] = useState('a new note...') 
  const [showAll, setShowAll] = useState(true)
  const [errorMessage,setErrorMessage] = useState(null)

  useEffect(()=>{
    console.log(`effect`)
    
    noteService.getAll()
      .then(response => {
        console.log(`axios response`)
        setNotes(response.data)
      })
      .catch(error =>{
        console.log(`axios error`)
        setErrorMessage(`Unable to fetch data from db.json`)
        setTimeout(() => {
            setErrorMessage(null)
        },5000)
      })
  },[])
  console.log(`render notes ${notes.length}`)
  const notesToShow = showAll ? notes : notes.filter(note => note.important)

  const addNote = (event) => {
    event.preventDefault()
    const newNoteObj = {
      content : newNote,
      important : Math.random() < 0.5,
    }    
    setNewNote('')
    noteService.create(newNoteObj)
      .then(response => {
        setNotes(notes.concat(response.data))
      }).catch(error => {
        setErrorMessage(`failed to add a note in db.json`)
        setTimeout(() => {
            setErrorMessage(null)
        },5000)
      })
  }
  const handleNoteChanges = (event) => {
    console.log(event.target.value)
    setNewNote(event.target.value)
  }
  const toggleImportance = (id)=>{
    console.log(`toggle importance ${id}`)
    const note = notes.find(n => n.id === id)
    const changedNote = { ...note, important: !note.important }
  
    noteService.update(id,changedNote)
      .then(response => {
        setNotes(notes.map(n => n.id !== id ? n : response.data))
      })
      .catch(error => {
        setErrorMessage('Unable to change note importance')
        setTimeout(() => {
          setErrorMessage(null)
        },5000)
      })
  }
  return (
    <div>
      <h1>Notes</h1>
      <Notification message={errorMessage}></Notification>
      <button onClick={() => setShowAll(!showAll)}>
        show {showAll ? 'important' : 'all'}
      </button>
      <ul>
        {notesToShow.map(note => 
          <Note key={note.id} note={note} toggleImportance={() => {toggleImportance(note.id)}} />
        )}
      </ul>
      <form onSubmit={addNote}>
        <input value={newNote} onChange={handleNoteChanges}/>
        <button type="submit">save</button>
      </form>
    </div>
  )
}

const Notification = ({ message }) => {
  if (message === null) {
    return null
  }

  return (
    <div className='error'>
      {message}
    </div>
  )
}

const Note = ({ note,toggleImportance }) => {
    
  const noteStyle = {
    color : 'red',
    paddingTop : 3,
    fontSize : 14,
  }
  const label = note.important ? 'Make not Important' : 'Make important'
    return (
      <li style={noteStyle}>
        {note.content}
        <button onClick={toggleImportance}>{label}</button>
      </li>
    )
}
  