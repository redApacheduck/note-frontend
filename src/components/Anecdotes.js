import { useState } from "react"

export const Anecdotes = () => {
    const anecdotes = [
        'If it hurts, do it more often.',
        'Adding manpower to a late software project makes it later!',
        'The first 90 percent of the code accounts for the first 10 percent of the development time...The remaining 10 percent of the code accounts for the other 90 percent of the development time.',
        'Any fool can write code that a computer can understand. Good programmers write code that humans can understand.',
        'Premature optimization is the root of all evil.',
        'Debugging is twice as hard as writing the code in the first place. Therefore, if you write the code as cleverly as possible, you are, by definition, not smart enough to debug it.',
        'Programming without an extremely heavy use of console.log is same as if a doctor would refuse to use x-rays or blood tests when diagnosing patients.',
        'The only way to go fast, is to go well.'
      ]
      
      const [anecdoteSelectedIndex, setAnecdoteSelectedIndex] = useState(0)
      const [votes,setVote] = useState({})
      const getMostVoted = (anecdotesVotes,anecdotesText) => {
        if(Object.entries(anecdotesVotes).length !==0){
          const result = Object.entries(anecdotesVotes).reduce((result,entry)=>{
            if(entry[1] > result[1]){
              return entry
            } else {
              return result
            }
          },[0,0])
          return anecdotesText[result[0]]      
        }else{
          return null
        }
      }
    return (
        <div>
            <Anecdote
        anecdote={anecdotes[anecdoteSelectedIndex]}
        showAnother={() => setAnecdoteSelectedIndex(Math.floor(Math.random() * (anecdotes.length)))}
        vote={() => {
          const updatedVotes = {...votes}
          if(!updatedVotes[anecdoteSelectedIndex]){
            updatedVotes[anecdoteSelectedIndex] = 1
          }else{
            updatedVotes[anecdoteSelectedIndex] = votes[anecdoteSelectedIndex] + 1
          }
          setVote(updatedVotes)
        }}
        voteCount={votes[anecdoteSelectedIndex] ? votes[anecdoteSelectedIndex] : 0}
      ></Anecdote> 
      <MostVotedAnecdote anecdote={getMostVoted(votes,anecdotes)}></MostVotedAnecdote>  
        </div>
    )
}


const Anecdote = ({anecdote,showAnother,vote,voteCount}) => {
    return(
      <p>
        {anecdote}
        <br></br>
        has {voteCount} vote
        <br></br>
        <button onClick={showAnother}>Next anecdote</button>
        <br></br>
        <button onClick={vote}>Vote</button>
      </p>
    )
  }
  
  const MostVotedAnecdote = ({anecdote}) => {
    if(anecdote){
      return (
        <p>
          <h1>Anecdote with most votes</h1>
          {anecdote}
        </p>
      )
    }
  }