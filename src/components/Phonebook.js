import { useEffect, useState } from 'react'
import phonebook from './../services/phonebook'
export const Phonebook = () => {
    const [persons, setPersons] = useState([]) 
    const [newContact, setNewContact] = useState({
        name : '',
        phone : ''
    })
    const [filterQuery,setFilterQuery] = useState('')
    const [contactsToShow,setContactsToShow] = useState(persons)
    const [notificationMessage,setNotificationMessageString] = useState(null)
    const setNotification = (message,isError) => {
        setNotificationMessageString({
            text : message,
            isError : isError,
        })
        setTimeout(() => {
            setNotificationMessageString(null)
        },5000)
    }
    const addNameToPhoneBook = (event) => {
        event.preventDefault()
        console.log(`called addNameToPhoneBook ${JSON.stringify(newContact)}`)
        // if(persons.findIndex(person => person.name === newContact.name) !== -1){
            // setNotification(`${newContact.name} is already added to phonebook`,true)
        if(newContact.name === '' || newContact.name === null){
            setNotification(`Empty name cannot be added to phonebook`,true)
        // }else if(persons.findIndex(person => person.phone === newContact.phone) !== -1 ){
            // setNotification(`${newContact.phone} is already added to phonebook`,true)
        }else if(newContact.phone === '' || newContact.phone === null){
            setNotification(`Empty phone cansnot be added to phonebook`,true)
        }else{
            console.log(`Called newContact Add ${JSON.stringify(newContact)}`)
            phonebook
                .create(newContact)
                .then(resultNewContact => {
                    setFilterQuery('')
                    setPersons(persons.concat(resultNewContact.data))
                    setNotification(`Added ${resultNewContact.data.name}`,false)
                })
                .catch(error => {
                    setNotification(`Phone cannot be added to phonebook ${error}`,true)
                })
        }
    }
    useEffect(()=>{
        phonebook
            .getAll()
            .then(response => {
                setPersons(response.data)    
            }).catch(error =>{
                setNotification(`Unable to fetch data from persons.json`,true)
                setTimeout(() => {
                    setNotification(null)
                },5000)
            })
    },[])

    return (
    <div>
        <h2>Phonebook</h2>
            <Filter filterQuery={filterQuery} persons={persons} setFilterQuery={setFilterQuery} setContactsToShow={setContactsToShow}></Filter>
        <Notification message={notificationMessage}></Notification>
        <h2>Add a new</h2>
           <AddContact addNameToPhoneBook={addNameToPhoneBook} newContact={newContact} setNewContact={setNewContact}></AddContact>
        <h2>Numbers</h2>
            {contactsToShow.map(person => <Name key={person.id} person={person}></Name>)}
    </div>
    )
}

const Notification = ({ message }) => {
    if (message === null) {
      return null
    }
  
    if(message.isError === true){
        return (<div className='failNotification'>{message.text}</div>)
    }else{
        return (<div className='successNotification'>{message.text}</div>)
    }
}

const AddContact = ({addNameToPhoneBook,newContact,setNewContact}) => {
    return (
        <form onSubmit={addNameToPhoneBook}>
        <div>
            name:
            <input 
                value={newContact.name} 
                onChange={(event) => setNewContact({
                    ...newContact,
                    name : event.target.value
                })}
            />
        </div>
        <div>
            phone:
            <input 
                type='number'
                value={newContact.phone}
                onChange={(event)=>{
                    setNewContact({
                        ...newContact,
                        phone : event.target.value
                    })}
                }>
            </input>
        </div>
        <button type="submit">add</button>
    </form>
    )
}
const Filter = ({filterQuery,persons,setFilterQuery,setContactsToShow}) =>{
    return (
        <form>
                filter shown with:
                <input
                    value={filterQuery}
                    onChange={(event)=>{
                        setFilterQuery(event.target.value)
                        if(event.target.value !== ''){
                            setContactsToShow(
                                persons.filter(person => person.name.toLowerCase().includes(event.target.value.toLocaleLowerCase()))
                            )
                        }else{
                            setContactsToShow(persons)
                        }
                    }}
                >
                </input>
            </form>
    )
}
const Name = ({person}) =>{
    return (
        <li class="phoneBookItem">
            {person.name} - {person.phone}
        </li>
    )
}